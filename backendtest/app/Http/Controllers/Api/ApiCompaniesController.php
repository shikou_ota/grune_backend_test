<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Prefecture;
use App\Models\Postcode;

class ApiCompaniesController extends Controller {

    /**
     * Return the contents of User table in tabular form
     *
     */
    public function getCompaniesTabular() {
        $company = Company::orderBy('id', 'desc')->get();
        // $prefecture = Prefecture::find();
        for ($i=0; $i < count($company); $i++) { 
            $prefecture = Prefecture::find($company[$i]->prefecture_id);
            $company[$i]->prefecture = "$prefecture->display_name";
        }

        return response()->json($company);
    }
    
    public function getAddressByPostalCode($postcode)
    {
        $addresses = Postcode::where('postcode', $postcode)->get();
        $prefecture = Prefecture::where('display_name', $addresses[0]->prefecture)->get();
        $addresses[0] -> pre_num = $prefecture[0]->id ;
        return response()->json($addresses);
    }



}
