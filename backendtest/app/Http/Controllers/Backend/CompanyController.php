<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Prefecture;
use App\Models\Postcode;
use Config;

class CompanyController extends Controller {

    /**
     * Get named route
     *
     */
    private function getRoute() {
        return 'company';
    }

    /**
     * Validator for user
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data, $type)
    {
        // Determine if password validation is required depending on the calling
        if ($type == "create") {
            return Validator::make($data, [
                'name' => 'required|string|max:100' . $data['id'],
                'email' => 'required|string|max:255',
                'postcode' => 'required|numeric',
                'prefecture_id' => 'required',
                'city' => 'required|string|max:255',
                'local'=>'required|string|max:255',
                'image'=>'required|image|max:5120',
            ]);
        } else {
            return Validator::make($data, [
                'name' => 'required|string|max:100' . $data['id'],
                'email' => 'required|string|max:255',
                'postcode' => 'required|numeric',
                'prefecture_id' => 'required|int',
                'city' => 'required|string|max:255',
                'local'=>'required|string|max:255',
                'image'=>'image|max:5120',
            ]);
        }
    }

    public function index()
    {
        return view('backend.company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $company = new Company();
        $company->form_action = $this->getRoute() . '.create';
        $company->page_title = 'Company Add Page';
        $company->page_type = 'create';
        // セレクトボックスの県選択
        $pre = Prefecture::orderBy('id', 'asc')->get(['display_name']);
        $arr = [""];
        for ($i=0; $i < 47; $i++) {
            array_push($arr, $pre[$i]->display_name);
        }


        return view('backend.company.form', [
            'company' => $company,
            "arr" => $arr,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $newCompany = $request->all();
        
        $this->validator($newCompany, 'create')->validate();  
        
        try {
            $company = Company::create($newCompany);
            if ($request->file('image')) {
                $company->update($newCompany);
                $file = $request->file('image');
                $fileName = $file->getClientOriginalExtension();
                $company->image = "image_$company->id.$fileName";
                $company->save();

                $filename = $request->file('image')->storeAs(
                    'public/files', $company->image
                );
            }


            // If update is successful
            return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_UPDATE_MESSAGE'));


        } catch (Exception $e) {
            // Create is failed
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_CREATE_MESSAGE'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $company = Company::find($id);
        $company->form_action = $this->getRoute() . '.update';
        $company->page_title = 'Company Edit Page';
        // Add page type here to indicate that the form.blade.php is in 'edit' mode
        $company->page_type = 'edit';

        $pre = Prefecture::orderBy('id', 'asc')->get(['display_name']);
        $arr = [""];
        for ($i=0; $i < 47; $i++) {
            array_push($arr, $pre[$i]->display_name);
        }
        // array_combine($keys, $values);
        return view('backend.company.form', [
            'company' => $company,
            "arr" => $arr,
        ]);
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $newCompany = $request->all();
        try {
                $company = Company::find($request->get('id'));
            
                $this->validator($newCompany, 'update')->validate();
                $company->update($newCompany);

                if ($request->file('image')) {
                    $file = $request->file('image');
                    $fileName = $file->getClientOriginalExtension();
                    $company->image = "image_$request->id.$fileName";
                    $company->save();

                    $filename = $request->file('image')->storeAs(
                        'public/files', $company->image
                    );
                }
                
                // If update is successful
                return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_UPDATE_MESSAGE'));
            
            
        } catch (Exception $e) {
            // If update is failed
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_UPDATE_MESSAGE'));
        }
    }

    public function delete(Request $request)
    {
        try {
            // Get user by id
            $company = Company::find($request->get('id'));

            // Delete user
            $company->delete();

            // If delete is successful
            return redirect()->route($this->getRoute())->with('success', Config::get('const.SUCCESS_DELETE_MESSAGE'));
            
            // Send error if logged in user trying to delete himself
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_DELETE_SELF_MESSAGE'));
        } catch (Exception $e) {
            // If delete is failed
            return redirect()->route($this->getRoute())->with('error', Config::get('const.FAILED_DELETE_MESSAGE'));
        }
    }

}

?>