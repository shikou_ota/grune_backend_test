$(function () {
    // init: side menu for current page
    $('li#menu-companies').addClass('menu-open active');
    $('li#menu-companies').find('.treeview-menu').css('display', 'block');
    $('li#menu-companies').find('.treeview-menu').find('.add-companies a').addClass('sub-menu-active');

    $('#user-form').validationEngine('attach', {
        promptPosition : 'topLeft',
        scroll: false
    });

    // init: show tooltip on hover
    $('[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    // show password field only after 'change password' is clicked
    $('#reset-button').click(function (e) {
        $('#reset-field').removeClass('hide');
        $('#show-password-check').removeClass('hide');
        // to always uncheck the checkbox after button click
        $('#show-password').prop('checked', false);
    });

    // toggle password in plaintext if checkbox is selected
    $("#show-password").click(function () {
        $(this).is(":checked") ? $("#password").prop("type", "text") : $("#password").prop("type", "password");
    });

    document.querySelectorAll("option")[0].style.display = "none";

    // input[file]の画像を表示
    $('#file-sample input').on('change', function(e) {
        // 1枚だけ表示する
        var file = e.target.files[0];

        // ファイルのブラウザ上でのURLを取得する
        var blobUrl = window.URL.createObjectURL(file);

        // img要素に表示
        $('#file-preview').attr('src', blobUrl);
    });

    // 検索欄数字の入力のみを許可する
    $('#postal_code').on("keypress", function(event){return leaveOnlyNumber(event);});
    function leaveOnlyNumber(e){
        var st = String.fromCharCode(e.which);
        if ("0123456789".indexOf(st,0) < 0) { 
            alert("半角数字のみ入力できます。");
            return false; 
        }
        return true;  
    }
    // 住所自動入力
    $('#get_address').on('click', function(){
        var postal_code = $('#postal_code').val();
        var request = $.ajax({
            type: 'GET',
            url: `${window.location.protocol}//${window.location.host}/api/admin/companies/getPostcode/${postal_code}`,
            cache: false,
            dataType: 'json',
            timeout: 10000
        });
        console.log(postal_code);

        /* 成功時 */
        request.done(function(data){
            console.log(data[0].id,data);
            $("#Prefecture_input").val(data[0].pre_num).change();
            $("#city_input").val(data[0].city);
            $("#local_input").val(data[0].local);
            $("#streetAddress_input").val(`${data[0].prefecture}${data[0].city}${data[0].local}`);
        });

        /* 失敗時 */
        request.fail(function(){
            alert(`通信に失敗しました${postal_code}`);
        });

    });
});
