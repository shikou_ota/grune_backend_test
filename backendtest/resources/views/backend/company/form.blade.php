@extends('backend/layout')
@section('content')
<section class="content-header">
    <h1>Company</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ $company->page_title }}</li>
    </ol>
</section>
<!-- Main content -->
<section id="main-content" class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{{ $company->page_title }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    {{ Form::open(array('route' => $company->form_action, 'method' => 'POST', 'files' => true, 'id' => 'company-form')) }}
                    {{ Form::hidden('id', $company->id, array('id' => 'company_id')) }}
                        <div id="form-name" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <span class="label label-danger label-required">Required</span>
                                <strong class="field-title">Name</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('name', $company->name, array('placeholder' => 'Grune Asia','class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-email" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <span class="label label-danger label-required">Required</span>
                                <strong class="field-title">Email</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::email('email', $company->email, array('placeholder' => 'info@grune.co.jp', 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-postcode" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <span class="label label-danger label-required">Required</span>
                                <strong class="field-title">Postcode</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content postcode-box">
                                {{ Form::number('postcode', $company->postcode, array('placeholder' => 'only number(半角)', "id"=>"postal_code",'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                                <button type="button" class="btn btn-primary" id="get_address">Search</button>
                            </div>
                        </div>

                        <div id="form-prefecture_id" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <span class="label label-danger label-required">Required</span>
                                <strong class="field-title">Prefecture</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                @if($company->page_type == 'create')
                                    {{ Form::select(
                                            'prefecture_id',
                                            [$arr],
                                            // ["1"=>"北海道","2"=>"青森県"],
                                            null,
                                            array( 'placeholder' => null,"id"=>"Prefecture_input",'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')
                                        )
                                    }}
                                @else
                                    {{ Form::select(
                                            'prefecture_id',
                                            [$arr],
                                            // ["1"=>"北海道","2"=>"青森県"],
                                            $company->prefecture_id,
                                            array( 'placeholder' => null,"id"=>"Prefecture_input",'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')
                                        )
                                    }}
                                @endif
                            </div>
                        </div>

                        <div id="form-city" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <span class="label label-danger label-required">Required</span>
                                <strong class="field-title">City</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('city', $company->city, array('placeholder' => '',"id"=>"city_input", 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-local" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <span class="label label-danger label-required">Required</span>
                                <strong class="field-title">Local</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('local', $company->local, array('placeholder' => '',"id"=>"local_input", 'class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-street_address" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">Street Address</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('street_address', $company->street_address, array('placeholder' => '',"id"=>"streetAddress_input", 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-business_hour" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">Business Hour</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('business_hour', $company->business_hour, array('placeholder' => '', 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-regular_holiday" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">Regular Holiday</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('regular_holiday', $company->regular_holiday, array('placeholder' => '', 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>
                        
                        <div id="form-phone" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">Phone</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('phone', $company->phone, array('placeholder' => '', 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-fax" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">Fax</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('fax', $company->fax, array('placeholder' => '', 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-url" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">Url</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('url', $company->url, array('placeholder' => '', 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>

                        <div id="form-license_number" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                <strong class="field-title">License Number</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                {{ Form::text('license_number', $company->license_number, array('placeholder' => '', 'class' => 'form-control validate[maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                            </div>
                        </div>
                        
                        <div id="form-image" class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-header">
                                @if($company->page_type == 'create')
                                    <span class="label label-danger label-required">Required</span>
                                @endif
                                <strong class="field-title">Image</strong>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10 col-content">
                                @if($company->page_type == 'create')
                                    <div id="file-sample">
                                        {{ Form::file('image', null, array('class' => 'form-control validate[required, maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                                    </div>
                                    <p><img id="file-preview" class="no_image" src="{{ asset('img/no-image/no-image.jpg')}}"></p>
                                @else
                                    <div id="file-sample">
                                        {{ Form::file('image', null, array('class' => 'form-control validate[ maxSize[100]]', 'data-prompt-position' => 'bottomLeft:0,11')) }}
                                    </div>
                                    <p><img id="file-preview" class="no_image" src="{{ asset('uploads/files/'.$company->image)}}"></p>
                                @endif
                            </div>
                        </div>

                        

                        <div id="form-button" class="form-group no-border">
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="margin-top: 20px;">
                                <button type="submit" name="submit" id="send" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<!-- /.content -->
@endsection

@section('title', 'Comapany | ' . env('APP_NAME',''))

@section('body-class', 'custom-select')

@section('css-scripts')
@endsection

@section('js-scripts')
<script src="{{ asset('bower_components/bootstrap/js/tooltip.js') }}"></script>
<!-- validationEngine -->
<script src="{{ asset('js/3rdparty/validation-engine/jquery.validationEngine-en.js') }}"></script>
<script src="{{ asset('js/3rdparty/validation-engine/jquery.validationEngine.js') }}"></script>
<script src="{{ asset('js/backend/company/form.js') }}"></script>
@endsection
